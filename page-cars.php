<?php
/*
 * Template Name: Automobiles
 * description: This is custom post type template
 */
get_header();

$args = array(  
        'post_type' => 'automobiles',
        'post_status' => 'publish',
        'posts_per_page' => -1, 
        'orderby' => 'title', 
        'order' => 'ASC', 
    );

    $loop = new WP_Query( $args ); 
        
    while ( $loop->have_posts() ) : $loop->the_post(); 
         the_title('<h2 class="entry-title">', '</h2>'); 
    endwhile;

    wp_reset_postdata(); 

get_footer(); ?>