<!DOCTYPE html>
<html>
<head>
<title>Form</title>
</head>
<style>
label
{
	font-weight: 600;
}
.submit
{
	background: black;
	padding: 10px 20px;
	color: white;
	border: 2px solid black;
}
input
{
	margin: 5px;
	padding: 10px;
}
.span-error
{
	background: red;
	padding: 20px;
	width: 50%;
	color: white;
}
.error
{
	color: red;
}
</style>
<?php
// define variables and set to empty values
$nameErr1 = $nameErr2 = $nameErr3 = $healtherr1 = $healtherr2 = $healtherr3 = "";
$success = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name1"])) {
    $nameErr1 = "Name is required";
  } else if(strlen($_POST["name1"]) < 3 ) {
    $nameErr1 = "Name should be 3 char";
  }
  else
  {
	  $nameErr1 = "";
  }
  if (empty($_POST["name2"])) {
    $nameErr2 = "Name is required";
  } else if(strlen($_POST["name2"]) < 3 ) {
    $nameErr2 = "Name should be 3 char";
  }
  else
  {
	  $nameErr2 = "";
  }
  if (empty($_POST["name3"])) {
    $nameErr3 = "Name is required";
  } else if(strlen($_POST["name3"]) < 3 ) {
    $nameErr3 = "Name should be 3 char";
  }
  else
  {
	  $nameErr3 = "";
  }
  if (empty($_POST["healthid1"])) {
    $healtherr1 = "Healthid is required";
  } else if(strlen($_POST["healthid1"]) != 5 ) {
    $healtherr1 = "Healthid should be 5 digit";
  }
  else
  {
	  $healtherr1 = "";
  }
  if (empty($_POST["healthid2"])) {
    $healtherr2 = "Healthid is required";
  } else if(strlen($_POST["healthid2"]) != 5 ) {
    $healtherr2 = "Healthid should be 5 digit";
  }
  else
  {
	  $healtherr2 = "";
  }
   if (empty($_POST["healthid3"])) {
    $healtherr3 = "Healthid is required";
  } else if(strlen($_POST["healthid3"]) != 5) {
    $healtherr3 = "Healthid should be 5 digit";
  }
  else
  {
	  $healtherr3 = "";
  }
  if(empty($nameErr1) && empty($nameErr2) && empty($nameErr3) && empty($healtherr1) && empty($healtherr2) && empty($healtherr3))
  {
	  $success = "SUCCESS";
	  $_POST["name1"] = $_POST["name2"] = $_POST["name3"] = $_POST["healthid1"] = $_POST["healthid2"] = $_POST["healthid3"] = "";
  }
}
if(isset($success) && !empty($success))
{
?>
<body>
<div class="span-error"><?php echo $success; ?></div>
<?php } ?>
<h2>Please Fill the Form</h2>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  <b>Name1:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="name1" value="<?php echo isset($_POST["name1"]) ? $_POST["name1"] : ''; ?>">
  <span class="error"><?php echo $nameErr1;?></span>
  <br><br>
  <b>Healthid1:</b> <input type="number" name="healthid1" value="<?php echo isset($_POST["healthid1"]) ? $_POST["healthid1"] : ''; ?>">
  <span class="error"> <?php echo $healtherr1;?></span>
  <br><br>
  <b>Name2:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <input type="text" name="name2" value="<?php echo isset($_POST["name2"]) ? $_POST["name2"] : ''; ?>">
  <span class="error"> <?php echo $nameErr2;?></span>
  <br><br>
  <b>Healthid2:</b> <input type="number" name="healthid2" value="<?php echo isset($_POST["healthid2"]) ? $_POST["healthid2"] : ''; ?>">
  <span class="error"> <?php echo $healtherr2;?></span>
  <br><br>
  <b>Name3:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="name3" value="<?php echo isset($_POST["name3"]) ? $_POST["name3"] : ''; ?>">
  <span class="error"> <?php echo $nameErr3;?></span>
  <br><br>
   <b>Healthid3:</b> <input type="number" name="healthid3" value="<?php echo isset($_POST["healthid3"]) ? $_POST["healthid3"] : ''; ?>">
  <span class="error"><?php echo $healtherr3;?></span>
  <br><br>
  
  <input type="submit" name="submit" value="Submit" class="submit">  
</form>
</body>
</html>
